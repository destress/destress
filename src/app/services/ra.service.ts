import { Injectable } from '@angular/core';
import { F4 } from '../models/F4.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class RAService {
  ras: F4[] = [];
  raSubject = new Subject<F4[]>();


constructor() {}
  

  emitRA() {
    this.raSubject.next(this.ras);
  }
  saveRA() {
    firebase.database().ref('/s7/Ra').set(this.ras);
  }
  getRA() {
    firebase.database().ref('/s7/Ra')
    .on('value', (data) => {
      this.ras = data.val() ? data.val() : [];
      this.emitRA();
    }
  );
  }
 
  createNewRA(newRa: F4 ) {
    this.ras.push(newRa);
    this.saveRA();
    this.emitRA();
  } 
}
