import { Injectable } from '@angular/core';
import { F6 } from '../models/F6.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class QAService {
  qas: F6[] = [];
  qaSubject = new Subject<F6[]>();


constructor() {}
  

  emitQA() {
    this.qaSubject.next(this.qas);
  }
  saveQA() {
    firebase.database().ref('/s1/Qa').set(this.qas);
  }
  getQA() {
    firebase.database().ref('/s1/Qa')
    .on('value', (data) => {
      this.qas = data.val() ? data.val() : [];
      this.emitQA();
    }
  );
  }
  createNewQA(newBook: F6 ) {
    this.qas.push(newBook);
    this.saveQA();
    this.emitQA();
  }  
}
