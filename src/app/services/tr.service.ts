import { Injectable } from '@angular/core';
import { F5 } from '../models/F5.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class TRService {
  trs: F5[] = [];
  trSubject = new Subject<F5[]>();


constructor() {}
  

  emitTR() {
    this.trSubject.next(this.trs);
  }
  saveTR() {
    firebase.database().ref('/s7/Tr').set(this.trs);
  }
  getTR() {
    firebase.database().ref('/s7/Tr')
    .on('value', (data) => {
      this.trs = data.val() ? data.val() : [];
      this.emitTR();
    }
  );
  }
 
  createNewTR(newTr: F5 ) {
    this.trs.push(newTr);
    this.saveTR();
    this.emitTR();
  } 
}
