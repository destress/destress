import { Injectable } from '@angular/core';
import { F4 } from '../models/F4.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class STService {
  sts: F4[] = [];
  stSubject = new Subject<F4[]>();


constructor() {}
  

  emitST() {
    this.stSubject.next(this.sts);
  }
  saveST() {
    firebase.database().ref('/s1/St').set(this.sts);
  }
  getST() {
    firebase.database().ref('/s1/St')
    .on('value', (data) => {
      this.sts = data.val() ? data.val() : [];
      this.emitST();
    }
  );
  }
 
  createNewST(newST: F4 ) {
    this.sts.push(newST);
    this.saveST();
    this.emitST();
  } 
}
