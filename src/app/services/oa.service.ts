import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';
import { F1 } from '../models/F1.model';


@Injectable()

export class OAService {
  oas: F1[] = [];
  oaSubject = new Subject<F1[]>();


constructor() {}
  

  emitOA() {
    this.oaSubject.next(this.oas);
  }
  saveOA() {
    firebase.database().ref('/s6/OA').set(this.oas);
  }
  getOA() {
    firebase.database().ref('/s6/OA')
    .on('value', (data) => {
      this.oas = data.val() ? data.val() : [];
      this.emitOA();
    }
  );
  }
  
  createNewOA(newOA: F1 ) {
    this.oas.push(newOA);
    this.saveOA();
    this.emitOA();
  } 
}
