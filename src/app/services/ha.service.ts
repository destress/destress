import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';
import { F5 } from '../models/F5.model';


@Injectable()

export class HAService {
  has: F5[] = [];
  haSubject = new Subject<F5[]>();


constructor() {}
  

  emitHA() {
    this.haSubject.next(this.has);
  }
  saveHA() {
    firebase.database().ref('/s7/Ha').set(this.has);
  }
  getHA() {
    firebase.database().ref('/s7/Ha')
    .on('value', (data) => {
      this.has = data.val() ? data.val() : [];
      this.emitHA();
    }
  );
  }
  
  createNewHA(newHA: F5 ) {
    this.has.push(newHA);
    this.saveHA();
    this.emitHA();
  } 
}
