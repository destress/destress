import { Injectable } from '@angular/core';
import { F3 } from '../models/F3.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';

@Injectable()
export class LGService {
  lgs: F3[] = [];
  lgSubject = new Subject<F3[]>();

constructor() {
  this.getLG();
}  

  emitLG() {
    this.lgSubject.next(this.lgs);
  }
  saveLG() {
    firebase.database().ref('/s1/Lg').set(this.lgs);
  }
  getLG() {
    firebase.database().ref('/s1/Lg')
    .on('value', (data) => {
      this.lgs = data.val() ? data.val() : [];
      this.emitLG();
    }
  );
  }
  getSingleLG(id: number) {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/s1/LG/' + id).once('value').then(
          (data) => {
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
    }
    
  createNewLG(newLG: F3 ) {
    this.lgs.push(newLG);
    this.saveLG();
    this.emitLG();
  } 
}
