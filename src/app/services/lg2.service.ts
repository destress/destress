import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';
import { F5 } from '../models/F5.model';


@Injectable()

export class LG2Service {
  lgs: F5[] = [];
  lgSubject = new Subject<F5[]>();


constructor() {
  this.getLG();
}
  

  emitLG() {
    this.lgSubject.next(this.lgs);
  }
  saveLG() {
    firebase.database().ref('/s2/Lg').set(this.lgs);
  }
  getLG() {
    firebase.database().ref('/s2/Lg')
    .on('value', (data) => {
      this.lgs = data.val() ? data.val() : [];
      this.emitLG();
    }
  );
  }
  getSingleLG(id: number) {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/s2/LG/' + id).once('value').then(
          (data) => {
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
    }
  
  createNewLG(newLG: F5 ) {
    this.lgs.push(newLG);
    this.saveLG();
    this.emitLG();
  } 
}
