import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';
import { F3 } from '../models/F3.model';


@Injectable()

export class LG3Service {
  lgs: F3[] = [];
  lgSubject = new Subject<F3[]>();


constructor() {
  this.getLG();
}
  

  emitLG() {
    this.lgSubject.next(this.lgs);
  }
  saveLG() {
    firebase.database().ref('/s3/Lg').set(this.lgs);
  }
  getLG() {
    firebase.database().ref('/s3/Lg')
    .on('value', (data) => {
      this.lgs = data.val() ? data.val() : [];
      this.emitLG();
    }
  );
  }
  getSingleLG(id: number) {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/s3/LG/' + id).once('value').then(
          (data) => {
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
    }
  
  createNewLG(newLG: F3 ) {
    this.lgs.push(newLG);
    this.saveLG();
    this.emitLG();
  } 
}
