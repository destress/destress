import { Injectable } from '@angular/core';
import { F7 } from '../models/F7.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class RTService {
  rts: F7[] = [];
  rtSubject = new Subject<F7[]>();


constructor() {}
  

  emitRT() {
    this.rtSubject.next(this.rts);
  }
  saveRT() {
    firebase.database().ref('/s2/Rt').set(this.rts);
  }
  getSRT() {
    firebase.database().ref('/s2/Rt')
    .on('value', (data) => {
      this.rts = data.val() ? data.val() : [];
      this.emitRT();
    }
  );
  }
 
  createNewRT(newRT: F7 ) {
    this.rts.push(newRT);
    this.saveRT();
    this.emitRT();
  } 
}
