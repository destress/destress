import { Injectable } from '@angular/core';
import { F6 } from '../models/F6.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class MGService {
  mgs: F6[] = [];
  mgSubject = new Subject<F6[]>();


constructor() {}
  

  emitMG() {
    this.mgSubject.next(this.mgs);
  }
  saveMG() {
    firebase.database().ref('/s8/Mg').set(this.mgs);
  }
  getMG() {
    firebase.database().ref('/s8/Mg')
    .on('value', (data) => {
      this.mgs = data.val() ? data.val() : [];
      this.emitMG();
    }
  );
  }
  createNewMG(newMg: F6 ) {
    this.mgs.push(newMg);
    this.saveMG();
    this.emitMG();
  }  
}
