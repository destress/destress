import { Injectable } from '@angular/core';
import { F2 } from '../models/F2.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class BAService {
  bas: F2[] = [];
  baSubject = new Subject<F2[]>();


constructor() {}
  

  emitBA() {
    this.baSubject.next(this.bas);
  }
  saveBA() {
    firebase.database().ref('/s8/Ba').set(this.bas);
  }
  getBA() {
    firebase.database().ref('/s8/Ba')
    .on('value', (data) => {
      this.bas = data.val() ? data.val() : [];
      this.emitBA();
    }
  );
  }
 
  createNewBA(newBa: F2 ) {
    this.bas.push(newBa);
    this.saveBA();
    this.emitBA();
  } 
}
