import { Injectable } from '@angular/core';
import { F4 } from '../models/F4.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class ASService {
  ass: F4[] = [];
  asSubject = new Subject<F4[]>();


constructor() {}
  

  emitAS() {
    this.asSubject.next(this.ass);
  }
  saveAS() {
    firebase.database().ref('/s9/As').set(this.ass);
  }
  getAS() {
    firebase.database().ref('/s9/As')
    .on('value', (data) => {
      this.ass = data.val() ? data.val() : [];
      this.emitAS();
    }
  );
  }
 
  createNewAS(newAS: F4 ) {
    this.ass.push(newAS);
    this.saveAS();
    this.emitAS();
  } 
}
