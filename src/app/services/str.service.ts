import { Injectable } from '@angular/core';
import { F3 } from '../models/F3.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class STRService {
  strs: F3[] = [];
  strSubject = new Subject<F3[]>();


constructor() {}
  

  emitSTR() {
    this.strSubject.next(this.strs);
  }
  saveSTR() {
    firebase.database().ref('/s5/Str').set(this.strs);
  }
  getSTR() {
    firebase.database().ref('/s5/Str')
    .on('value', (data) => {
      this.strs = data.val() ? data.val() : [];
      this.emitSTR();
    }
  );
  }
 
  createNewSTR(newSTR: F3 ) {
    this.strs.push(newSTR);
    this.saveSTR();
    this.emitSTR();
  } 
}
