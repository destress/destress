import { Injectable } from '@angular/core';
import { F10 } from '../models/F10.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class GAService {
  gas: F10[] = [];
  gaSubject = new Subject<F10[]>();


constructor() {}
  

  emitGA() {
    this.gaSubject.next(this.gas);
  }
  saveGA() {
    firebase.database().ref('/s3/Ga').set(this.gas);
  }
  getGA() {
    firebase.database().ref('/s3/Ga')
    .on('value', (data) => {
      this.gas = data.val() ? data.val() : [];
      this.emitGA();
    }
  );
  }
 
  createNewGA(newGa: F10 ) {
    this.gas.push(newGa);
    this.saveGA();
    this.emitGA();
  } 
}
