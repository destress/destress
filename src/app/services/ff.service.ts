import { Injectable } from '@angular/core';
import { F4 } from '../models/F4.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';


@Injectable()

export class FFService {
  ffs: F4[] = [];
  ffSubject = new Subject<F4[]>();


constructor() {}
  

  emitFF() {
    this.ffSubject.next(this.ffs);
  }
  saveFF() {
    firebase.database().ref('/s7/Ff').set(this.ffs);
  }
  getFF() {
    firebase.database().ref('/s7/Ff')
    .on('value', (data) => {
      this.ffs = data.val() ? data.val() : [];
      this.emitFF();
    }
  );
  }
 
  createNewFF(newFF: F4 ) {
    this.ffs.push(newFF);
    this.saveFF();
    this.emitFF();
  } 
}
