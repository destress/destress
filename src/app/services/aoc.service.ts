import { Injectable } from '@angular/core';
import { F1 } from '../models/F1.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';

@Injectable()
export class AOCService {
  aos: F1[] = [];
  aoSubject = new Subject<F1[]>();

constructor() {}  

  emitAOC() {
    this.aoSubject.next(this.aos);
  }
  saveAOC() {
    firebase.database().ref('/s4/Aoc').set(this.aos);
  }
  getAOC() {
    firebase.database().ref('/s4/Aoc')
    .on('value', (data) => {
      this.aos = data.val() ? data.val() : [];
      this.emitAOC();
    }
  );
  }  
  createNewAOC(newAO: F1 ) {
    this.aos.push(newAO);
    this.saveAOC();
    this.emitAOC();
  } 
}
