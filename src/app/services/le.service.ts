import { Injectable } from '@angular/core';
import { F1 } from '../models/F1.model';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/internal/Subject';

@Injectable()
export class LEService {
  F1s: F1[] = [];
  F1Subject = new Subject<F1[]>();

constructor() {}  

  emitLE() {
    this.F1Subject.next(this.F1s);
  }
  saveLE() {
    firebase.database().ref('/s4/Le').set(this.F1s);
  }
  getLE() {
    firebase.database().ref('/s4/Le')
    .on('value', (data) => {
      this.F1s = data.val() ? data.val() : [];
      this.emitLE();
    }
  );
  }  
  createNewLE(newLE: F1 ) {
    this.F1s.push(newLE);
    this.saveLE();
    this.emitLE();
  } 
}
