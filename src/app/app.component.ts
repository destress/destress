import { Component } from '@angular/core';
import * as firebase from'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAQDRaIrTcS-Ka7t3luPvzzk3QH4a976Bk",
    authDomain: "destress-7d576.firebaseapp.com",
    databaseURL: "https://destress-7d576.firebaseio.com",
    projectId: "destress-7d576",
    storageBucket: "destress-7d576.appspot.com",
    messagingSenderId: "504320882465"
  };
  firebase.initializeApp(config);
  }
}
