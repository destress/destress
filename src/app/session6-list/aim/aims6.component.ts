import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-aims6',
  templateUrl: './aims6.component.html',
  styleUrls: ['./aims6.component.scss']
})
export class AimS6Component implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    // do init at here for current route.

    setTimeout(() => {
        this.router.navigate(['home/sessionM/session6']);
    }, 6000);  //5s
    }
}