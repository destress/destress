import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { F1 } from 'src/app/models/F1.model';
import { OAService } from 'src/app/services/oa.service';

@Component({
  selector: 'app-oa-form',
  templateUrl: './oa-form.component.html',
  styleUrls: ['./oa-form.component.scss']
})
export class OAFormComponent implements OnInit {

  oaForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private oaServices: OAService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.oaForm = this.formBuilder.group({
      r1: ['', Validators.required]
      
    });
  }
  onSaveOA() {
    const r1 = this.oaForm.get('r1').value;   
    const newOa = new F1(r1 );
    this.oaServices.createNewOA(newOa);
    this.router.navigate(['/home/sessionM/session6']);
    
  }
  
  

}
