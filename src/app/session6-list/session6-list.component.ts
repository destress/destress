import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-session6-list',
  templateUrl: './session6-list.component.html',
  styleUrls: ['./session6-list.component.scss']
})
export class Session6ListComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
  }  
  
  onNewOA() {
    this.router.navigate(['/home','sessionM','session6','newoa']);
  }
  
    
}  