import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AOCService } from 'src/app/services/aoc.service';
import { F1 } from 'src/app/models/F1.model';

@Component({
  selector: 'app-aoc-form',
  templateUrl: './aoc-form.component.html',
  styleUrls: ['./aoc-form.component.scss']
})
export class AOCFormComponent implements OnInit {

  aocForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private aocServices: AOCService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.aocForm = this.formBuilder.group({
      r1: ['', Validators.required]      
    });
  }


  onSaveAOC() {
    const r1 = this.aocForm.get('r1').value;    
    const newLe = new F1(r1);
    this.aocServices.createNewAOC(newLe);
    this.router.navigate(['/home/sessionM/session4']);
    
  }
  

  

  

}
