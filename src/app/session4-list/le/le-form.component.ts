import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LEService } from 'src/app/services/le.service';
import { F1 } from 'src/app/models/F1.model';

@Component({
  selector: 'app-le-form',
  templateUrl: './le-form.component.html',
  styleUrls: ['./le-form.component.scss']
})
export class LEFormComponent implements OnInit {

  leForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private leServices: LEService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.leForm = this.formBuilder.group({
      r1: ['', Validators.required]      
    });
  }
  

  onSaveLE() {
    const r1 = this.leForm.get('r1').value;    
    const newLe = new F1(r1);
    this.leServices.createNewLE(newLe);
    this.router.navigate(['/home/sessionM/session4']);
    
  }
  

  

  

}
