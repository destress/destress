import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-aims4-list',
  templateUrl: './aims4.component.html',
  styleUrls: ['./aims4.component.scss']
})
export class AimS4Component implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    // do init at here for current route.

    setTimeout(() => {
        this.router.navigate(['home/sessionM/session4']);
    }, 6000);  //5s
    }
} 