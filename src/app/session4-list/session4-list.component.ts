import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-session4-list',
  templateUrl: './session4-list.component.html',
  styleUrls: ['./session4-list.component.scss']
})
export class Session4ListComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
  }  
  
  
  onNewLE() {
    this.router.navigate(['/home','sessionM','session4','newle']);
  }
  onNewAOC() {
    this.router.navigate(['/home','sessionM','session4','newaoc']);
  }
  
    
}  