import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { F5 } from 'src/app/models/F5.model';
import { HAService } from 'src/app/services/ha.service';

@Component({
  selector: 'app-ha-form',
  templateUrl: './ha-form.component.html',
  styleUrls: ['./ha-form.component.scss']
})
export class HAFormComponent implements OnInit {

  haForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private haServices: HAService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.haForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required],
      r5: ['', Validators.required]
      
    });
  }


  onSaveHA() {
    const r1 = this.haForm.get('r1').value;
    const r2 = this.haForm.get('r2').value;
    const r3 = this.haForm.get('r3').value;
    const r4 = this.haForm.get('r4').value;
    const r5 = this.haForm.get('r5').value;
    const newHa = new F5(r1, r2, r3, r4, r5);
    this.haServices.createNewHA(newHa);
    this.router.navigate(['/home/sessionM/session7']);
    
  }
  

  

  

}
