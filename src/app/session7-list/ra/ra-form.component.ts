import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RAService } from 'src/app/services/ra.service';
import { F4 } from 'src/app/models/F4.model';

@Component({
  selector: 'app-ra-form',
  templateUrl: './ra-form.component.html',
  styleUrls: ['./ra-form.component.scss']
})
export class RAFormComponent implements OnInit {

  raForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private raServices: RAService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.raForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required]
      
    });
  }


  onSaveRA() {
    const r1 = this.raForm.get('r1').value;
    const r2 = this.raForm.get('r2').value;
    const r3 = this.raForm.get('r3').value;
    const r4 = this.raForm.get('r4').value;
    


    const newRa = new F4(r1, r2, r3, r4);
    this.raServices.createNewRA(newRa);
    this.router.navigate(['/home/sessionM/session7']);
    
  }

  

}
