import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, } from '@angular/forms';
import { Router } from '@angular/router';
import { F4 } from 'src/app/models/F4.model';
import { FFService } from 'src/app/services/ff.service';

@Component({
  selector: 'app-ff-form',
  templateUrl: './ff-form.component.html',
  styleUrls: ['./ff-form.component.scss']
})
export class FFFormComponent implements OnInit {

  ffForm: FormGroup;
  constructor(private formBuilder: FormBuilder, 
              private ffServices: FFService,
              private router: Router) { }
  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.ffForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required]
      
    });
  }

  onSaveFF() {
    const r1 = this.ffForm.get('r1').value;
    const r2 = this.ffForm.get('r2').value;
    const r3 = this.ffForm.get('r3').value;
    const r4 = this.ffForm.get('r4').value;
    const newFf = new F4(r1, r2, r3, r4);
    this.ffServices.createNewFF(newFf);
    this.router.navigate(['/home/sessionM/session7']);
    
  }

  

}
