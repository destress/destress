import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-aims7',
  templateUrl: './aims7.component.html',
  styleUrls: ['./aims7.component.scss']
})
export class AimS7Component implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    // do init at here for current route.

    setTimeout(() => {
        this.router.navigate(['home/sessionM/session7']);
    }, 6000);  //5s
    }
}