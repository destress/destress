import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { F5 } from 'src/app/models/F5.model';
import { TRService } from 'src/app/services/tr.service';

@Component({
  selector: 'app-tr-form',
  templateUrl: './tr-form.component.html',
  styleUrls: ['./tr-form.component.scss']
})
export class TRFormComponent implements OnInit {

  trForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private trServices: TRService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.trForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required],
      r5: ['', Validators.required]
      
    });
  }


  onSaveTR() {
    const r1 = this.trForm.get('r1').value;
    const r2 = this.trForm.get('r2').value;
    const r3 = this.trForm.get('r3').value;
    const r4 = this.trForm.get('r4').value;
    const r5 = this.trForm.get('r5').value;
    const newTr = new F5(r1, r2, r3, r4, r5);
    this.trServices.createNewTR(newTr);
    this.router.navigate(['/home/sessionM/session7']);
    
  }
}
