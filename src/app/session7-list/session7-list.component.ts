import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-session7-list',
  templateUrl: './session7-list.component.html',
  styleUrls: ['./session7-list.component.scss']
})
export class Session7ListComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
  }  
  
  onNewFF() {
    this.router.navigate(['/home','sessionM','session7','newff']);
  }
  onNewHA() {
    this.router.navigate(['/home','sessionM','session7','newha']);
  }
  onNewRA() {
    this.router.navigate(['/home','sessionM','session7','newra']);
  }
  onNewTR() {
    this.router.navigate(['/home','sessionM','session7','newtr']);
  }
  
    
}  