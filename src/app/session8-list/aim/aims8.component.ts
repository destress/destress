import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-aims8',
  templateUrl: './aims8.component.html',
  styleUrls: ['./aims8.component.scss']
})
export class AimS8Component implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    // do init at here for current route.

    setTimeout(() => {
        this.router.navigate(['home/sessionM/session8']);
    }, 6000);  //5s
    }
}