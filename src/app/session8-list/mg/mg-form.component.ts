import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MGService } from '../../services/mg.service';
import { Router } from '@angular/router';
import { F6 } from '../../models/F6.model';

@Component({
  selector: 'app-mg-form',
  templateUrl: './mg-form.component.html',
  styleUrls: ['./mg-form.component.scss']
})
export class MGFormComponent implements OnInit {

  mgForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private mgServices: MGService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.mgForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required],
      r5: ['', Validators.required],
      r6: ['', Validators.required]
      
    });
  }


  onSaveMG() {
    const r1 = this.mgForm.get('r1').value;
    const r2 = this.mgForm.get('r2').value;
    const r3 = this.mgForm.get('r3').value;
    const r4 = this.mgForm.get('r4').value;
    const r5 = this.mgForm.get('r5').value;
    const r6 = this.mgForm.get('r6').value;


    const newMg = new F6(r1, r2, r3, r4, r5, r6);
    this.mgServices.createNewMG(newMg);
    this.router.navigate(['/home/sessionM/session8']);
    
  }

  

}
