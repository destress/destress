import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-session8-list',
  templateUrl: './session8-list.component.html',
  styleUrls: ['./session8-list.component.scss']
})
export class Session8ListComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
  }  
  
  onNewMG() {
    this.router.navigate(['/home','sessionM','session8','newmg']);
  }
  onNewBA() {
    this.router.navigate(['/home','sessionM','session8','newba']);
  }
  
    
}  