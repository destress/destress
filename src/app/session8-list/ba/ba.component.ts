import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BAService } from '../../services/ba.service';
import { Router } from '@angular/router';
import { F2 } from '../../models/F2.model';

@Component({
  selector: 'app-ba',
  templateUrl: './ba.component.html',
  styleUrls: ['./ba.component.scss']
})
export class BAFormComponent implements OnInit {

  baForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private baServices: BAService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.baForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required]
      
    });
  }


  onSaveBA() {
    const r1 = this.baForm.get('r1').value;
    const r2 = this.baForm.get('r2').value;
    const newBa = new F2(r1, r2);
    this.baServices.createNewBA(newBa);
    this.router.navigate(['/home/sessionM/session8']);
    
  }

  

}
