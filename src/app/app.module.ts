//imports des modules : 
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
// import des components :
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AimS1Component } from './session-list/aim/aims1.component';
import { AimS2Component } from './session2-list/aim/aims2.component';  
import { AimS3Component } from './session3-list/aim/aims3.component';
import { AimS4Component } from './session4-list/aim/aims4.component';
import { AimS5Component } from './session5-list/aim/aims5.component';
import { AimS6Component } from './session6-list/aim/aims6.component';
import { AimS7Component } from './session7-list/aim/aims7.component';
import { AimS8Component } from './session8-list/aim/aims8.component';
import { AimS9Component } from './session9-list/aim/aims9.component';
import { SessionMListComponent } from './home/sessionMenu/sessionM.component';
import { SessionListComponent } from './session-list/session1-list.component';
import { Session2ListComponent } from './session2-list/session2-list.component';
import { Session3ListComponent } from './session3-list/session3-list.component';
import { Session4ListComponent } from './session4-list/session4-list.component';
import { Session5ListComponent } from './session5-list/session5-list.component';
import { Session6ListComponent } from './session6-list/session6-list.component';
import { Session7ListComponent } from './session7-list/session7-list.component';
import { Session8ListComponent } from './session8-list/session8-list.component';
import { Session9ListComponent } from './session9-list/session9-list.component';
import { LGFormComponent } from './session-list/lg/lg-form.component';
import { LG2FormComponent } from './session2-list/lg2/lg2-form.component';
import { QAFormComponent } from './session-list/qa/qa-form.component';
import { STFormComponent } from './session-list/st/st-form.component';
import { GAFormComponent } from './session3-list/ga/ga-form.component';
import { LG3FormComponent } from './session3-list/lg3/lg3-form.component';
import { AOCFormComponent } from './session4-list/aoc/aoc-form.component';

//imports des services :
import { AuthGuardService } from './services/auth-guard.service';
import { QAService } from './services/qa.service';
import { AuthService } from './services/auth.service';
import { STService } from './services/st.service';
import { LGService } from './services/lg.service';
import { LG2Service } from './services/lg2.service';
import { RTService } from './services/rt.service';
import { RTFormComponent } from './session2-list/rt/rt-form.component';
import { GAService } from './services/ga.service';
import { LG3Service } from './services/lg3.service';
import { LEFormComponent } from './session4-list/le/le-form.component';
import { LEService } from './services/le.service';
import { AOCService } from './services/aoc.service';
import { STRFormComponent } from './session5-list/str/str-form.component';
import { STRService } from './services/str.service';
import { OAService } from './services/oa.service';
import { OAFormComponent } from './session6-list/oa/oaform.component';
import { FFFormComponent } from './session7-list/ff/ff-form.component';
import { FFService } from './services/ff.service';
import { HAFormComponent } from './session7-list/ha/ha-form.component';
import { HAService } from './services/ha.service';
import { RAFormComponent } from './session7-list/ra/ra-form.component';
import { RAService } from './services/ra.service';
import { TRFormComponent } from './session7-list/tr/tr-form.component';
import { TRService } from './services/tr.service';
import { MGFormComponent } from './session8-list/mg/mg-form.component';
import { MGService } from './services/mg.service';
import { ASFormComponent } from './session9-list/as/as-form.component';
import { ASService } from './services/as.service';
import { HCComponent } from './session9-list/hc/hc.component';
import { SSComponent } from './session9-list/ss/ss.component';
import { REComponent } from './session5-list/re/re.component';
import { BAFormComponent } from './session8-list/ba/ba.component';
import { BAService } from './services/ba.service';
import { LogbookMComponent } from './home/logbookMenu/logbookM.component';

 

const appRoutes: Routes = [ 
  { path: 'auth/signup', component: SignupComponent },
  { path: 'auth/signin', component: SigninComponent },
  { path: 'home', canActivate:[AuthGuardService], component: HomeComponent },
  { path: 'home/sessionM', canActivate:[AuthGuardService], component: SessionMListComponent },
  { path: 'home/logbookM', canActivate:[AuthGuardService], component: LogbookMComponent },
  { path: 'home/sessionM/aimS1', canActivate:[AuthGuardService], component: AimS1Component },
  { path: 'home/sessionM/aimS2', canActivate:[AuthGuardService], component: AimS2Component },
  { path: 'home/sessionM/aimS3', canActivate:[AuthGuardService], component: AimS3Component },
  { path: 'home/sessionM/aimS4', canActivate:[AuthGuardService], component: AimS4Component },
  { path: 'home/sessionM/aimS5', canActivate:[AuthGuardService], component: AimS5Component },
  { path: 'home/sessionM/aimS6', canActivate:[AuthGuardService], component: AimS6Component },
  { path: 'home/sessionM/aimS7', canActivate:[AuthGuardService], component: AimS7Component },
  { path: 'home/sessionM/aimS8', canActivate:[AuthGuardService], component: AimS8Component },
  { path: 'home/sessionM/aimS9', canActivate:[AuthGuardService], component: AimS9Component },
  { path: 'home/sessionM/session1', canActivate:[AuthGuardService], component: SessionListComponent },
  { path: 'home/sessionM/session1/newqa', canActivate:[AuthGuardService], component: QAFormComponent },
  { path: 'home/sessionM/session1/newst', canActivate:[AuthGuardService], component: STFormComponent },
  { path: 'home/sessionM/session1/newlg', canActivate:[AuthGuardService], component: LGFormComponent },
  { path: 'home/sessionM/session2', canActivate:[AuthGuardService], component: Session2ListComponent },
  { path: 'home/sessionM/session2/newlg', canActivate:[AuthGuardService], component: LG2FormComponent },
  { path: 'home/sessionM/session2/newrt', canActivate:[AuthGuardService], component: RTFormComponent },
  { path: 'home/sessionM/session3', canActivate:[AuthGuardService], component: Session3ListComponent },
  { path: 'home/sessionM/session3/newga', canActivate:[AuthGuardService], component: GAFormComponent },
  { path: 'home/sessionM/session3/newlg', canActivate:[AuthGuardService], component: LG3FormComponent },
  { path: 'home/sessionM/session4', canActivate:[AuthGuardService], component: Session4ListComponent },
  { path: 'home/sessionM/session4/newle', canActivate:[AuthGuardService], component: LEFormComponent },
  { path: 'home/sessionM/session4/newaoc', canActivate:[AuthGuardService], component: AOCFormComponent },
  { path: 'home/sessionM/session5', canActivate:[AuthGuardService], component: Session5ListComponent },
  { path: 'home/sessionM/session5/newstr', canActivate:[AuthGuardService], component: STRFormComponent },
  { path: 'home/sessionM/session5/newre', canActivate:[AuthGuardService], component: REComponent },
  { path: 'home/sessionM/session6', canActivate:[AuthGuardService], component: Session6ListComponent },
  { path: 'home/sessionM/session6/newoa', canActivate:[AuthGuardService], component: OAFormComponent },
  { path: 'home/sessionM/session7', canActivate:[AuthGuardService], component: Session7ListComponent },
  { path: 'home/sessionM/session7/newff', canActivate:[AuthGuardService], component: FFFormComponent },
  { path: 'home/sessionM/session7/newha', canActivate:[AuthGuardService], component: HAFormComponent },
  { path: 'home/sessionM/session7/newra', canActivate:[AuthGuardService], component: RAFormComponent },
  { path: 'home/sessionM/session7/newtr', canActivate:[AuthGuardService], component: TRFormComponent },
  { path: 'home/sessionM/session8', canActivate:[AuthGuardService], component: Session8ListComponent },
  { path: 'home/sessionM/session8/newmg', canActivate:[AuthGuardService], component: MGFormComponent },
  { path: 'home/sessionM/session8/newba', canActivate:[AuthGuardService], component: BAFormComponent },
  { path: 'home/sessionM/session9', canActivate:[AuthGuardService], component: Session9ListComponent },
  { path: 'home/sessionM/session9/newas', canActivate:[AuthGuardService], component: ASFormComponent },
  { path: 'home/sessionM/session9/newhc', canActivate:[AuthGuardService], component: HCComponent },
  { path: 'home/sessionM/session9/newss', canActivate:[AuthGuardService], component: SSComponent },
  { path: '', redirectTo:'home', pathMatch: 'full' },
  { path:'**', redirectTo:'home' }
];

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    HomeComponent,
    HeaderComponent,
    SessionListComponent,
    Session2ListComponent,
    Session3ListComponent,
    Session4ListComponent,
    Session5ListComponent,
    Session6ListComponent,
    Session7ListComponent,
    Session8ListComponent,
    Session9ListComponent,
    SessionMListComponent,
    LogbookMComponent,
    AimS1Component,
    AimS2Component,
    AimS3Component,
    AimS4Component,
    AimS5Component,
    AimS6Component,
    AimS7Component,
    AimS8Component,
    AimS9Component,    
    QAFormComponent,    
    STFormComponent,
    RTFormComponent,
    GAFormComponent,
    LEFormComponent,
    AOCFormComponent,
    STRFormComponent,
    OAFormComponent,
    FFFormComponent,
    HAFormComponent,
    RAFormComponent,
    TRFormComponent,
    MGFormComponent,
    ASFormComponent,
    HCComponent,
    SSComponent,
    REComponent,
    BAFormComponent,
    LGFormComponent, 
    LG2FormComponent,
    LG3FormComponent
        
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
],
  providers: [
    AuthService,
    AuthGuardService,
    QAService,
    STService,
    RTService,
    GAService,
    LEService,
    AOCService,
    STRService,
    OAService,
    FFService,
    HAService,
    RAService,
    TRService,
    MGService, 
    ASService,
    BAService,
    LGService,
    LG2Service,
    LG3Service    
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
