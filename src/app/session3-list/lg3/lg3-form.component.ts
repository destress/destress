import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { F3 } from 'src/app/models/F3.model';
import { LG3Service } from 'src/app/services/lg3.service';

@Component({
  selector: 'app-lg3-form',
  templateUrl: './lg3-form.component.html',
  styleUrls: ['./lg3-form.component.scss']
})
export class LG3FormComponent implements OnInit {

  lg3Form: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private lgServices: LG3Service,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.lg3Form = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required]
      
    });
  }


  onSaveLG() {
    const r1 = this.lg3Form.get('r1').value;
    const r2 = this.lg3Form.get('r2').value;
    const r3 = this.lg3Form.get('r3').value;
    const newLg = new F3(r1, r2, r3,);
    this.lgServices.createNewLG(newLg);
    this.router.navigate(['/home/sessionM/session3']);
    
  }
  

  

  

}
