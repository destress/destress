import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-aims3',
  templateUrl: './aims3.component.html',
  styleUrls: ['./aims3.component.scss']
})
export class AimS3Component implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    // do init at here for current route.

    setTimeout(() => {
        this.router.navigate(['home/sessionM/session3']);
    }, 6000);  //5s
    }
}