import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-session3-list',
  templateUrl: './session3-list.component.html',
  styleUrls: ['./session3-list.component.scss']
})
export class Session3ListComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
  }  
  
  onNewGA() {
    this.router.navigate(['/home','sessionM','session3','newga']);
  }
  onNewLG() {
    this.router.navigate(['/home','sessionM','session3','newlg']);
  }
  
    
}  