import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { F10 } from 'src/app/models/F10.model';
import { GAService } from 'src/app/services/ga.service';

@Component({
  selector: 'app-ga-form',
  templateUrl: './ga-form.component.html',
  styleUrls: ['./ga-form.component.scss']
})
export class GAFormComponent implements OnInit {

  gaForm: FormGroup;
  constructor(private formBuilder: FormBuilder, 
              private gaServices: GAService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.gaForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required],
      r5: ['', Validators.required],
      r6: ['', Validators.required],
      r7: ['', Validators.required],
      r8: ['', Validators.required],
      r9: ['', Validators.required],
      r10: ['', Validators.required]      
    });
  }
  onSaveGA() {
    const r1 = this.gaForm.get('r1').value;
    const r2 = this.gaForm.get('r2').value;
    const r3 = this.gaForm.get('r3').value;
    const r4 = this.gaForm.get('r4').value;
    const r5 = this.gaForm.get('r5').value;
    const r6 = this.gaForm.get('r6').value;
    const r7 = this.gaForm.get('r7').value;
    const r8 = this.gaForm.get('r8').value;
    const r9 = this.gaForm.get('r9').value;
    const r10 = this.gaForm.get('r10').value;
    const newGa = new F10(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10);
    this.gaServices.createNewGA(newGa);
    this.router.navigate(['/home/sessionM/session3']);    
  }
}
