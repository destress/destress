import { Component, OnDestroy, OnInit } from '@angular/core';
import { QAService } from '../services/qa.service';
import { F6 } from '../models/F6.model';
import { Subscription } from 'rxjs/internal/Subscription';
import { Router } from '@angular/router';
import { F4 } from '../models/F4.model';
import { STService } from '../services/st.service';
import { F3 } from '../models/F3.model';
import { LGService } from '../services/lg.service';

@Component({
  selector: 'app-session1-list',
  templateUrl: './session1-list.component.html',
  styleUrls: ['./session1-list.component.scss']
})
export class SessionListComponent implements OnInit, OnDestroy {

  qas: F6[];
  qaSubscription: Subscription;
  sts: F4[];
  stSubscription: Subscription;
  lgs: F3[];
  lgSubscription: Subscription;
  

  constructor(private qaService: QAService,
              private stService: STService,
              private lgService: LGService,
               private router: Router) {}

  ngOnInit() {
    this.qaSubscription = this.qaService.qaSubject.subscribe(
      (qas: F6[]) => {
        this.qas = qas;
      }
    );
    this.qaService.getQA();
    this.qaService.emitQA();
    this.stSubscription = this.stService.stSubject.subscribe(
      (sts: F4[]) => {
        this.sts = sts;
      }
    );
    this.stService.getST();
    this.stService.emitST();
    this.lgSubscription = this.lgService.lgSubject.subscribe(
      (lgs: F3[]) => {
        this.lgs = lgs;
      }
    );
    this.lgService.getLG();
    this.lgService.emitLG();
  }
  ngOnDestroy() {
    this.qaSubscription.unsubscribe();
    this.stSubscription.unsubscribe();
    this.lgSubscription.unsubscribe();

  }
  onNewQA() {
    this.router.navigate(['/home','sessionM','session1', 'newqa']);
  }  
   
  onNewST() {
    this.router.navigate(['/home','sessionM','session1', 'newst']);
  }
  
  onNewLG() {
    this.router.navigate(['/home','sessionM','session1', 'newlg']);
  }
   
    
}  