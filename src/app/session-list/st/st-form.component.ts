import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, } from '@angular/forms';
import { Router } from '@angular/router';
import { STService } from 'src/app/services/st.service';
import { F4 } from 'src/app/models/F4.model';

@Component({
  selector: 'app-st-form',
  templateUrl: './st-form.component.html',
  styleUrls: ['./st-form.component.scss']
})
export class STFormComponent implements OnInit {

  stForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private stServices: STService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.stForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required]
      
    });
  }


  onSaveST() {
    const r1 = this.stForm.get('r1').value;
    const r2 = this.stForm.get('r2').value;
    const r3 = this.stForm.get('r3').value;
    const r4 = this.stForm.get('r4').value;
    


    const newSt = new F4(r1, r2, r3, r4);
    this.stServices.createNewST(newSt);
    this.router.navigate(['/home/sessionM/session1']);
    
  }

  

}
