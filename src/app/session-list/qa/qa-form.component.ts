import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { QAService } from '../../services/qa.service';
import { Router } from '@angular/router';
import { F6 } from '../../models/F6.model';

@Component({
  selector: 'app-session1-form',
  templateUrl: './qa-form.component.html',
  styleUrls: ['./qa-form.component.scss']
})
export class QAFormComponent implements OnInit {

  qaForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private qaServices: QAService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.qaForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required],
      r5: ['', Validators.required],
      r6: ['', Validators.required]
      
    });
  }


  onSaveQA() {
    const r1 = this.qaForm.get('r1').value;
    const r2 = this.qaForm.get('r2').value;
    const r3 = this.qaForm.get('r3').value;
    const r4 = this.qaForm.get('r4').value;
    const r5 = this.qaForm.get('r5').value;
    const r6 = this.qaForm.get('r6').value;
    const newQa = new F6(r1, r2, r3, r4, r5, r6);
    this.qaServices.createNewQA(newQa);
    this.router.navigate(['/home/sessionM/session1']);
    
  }

  

}
