import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, } from '@angular/forms';
import { Router } from '@angular/router';
import { LGService } from 'src/app/services/lg.service';
import { F3 } from 'src/app/models/F3.model';

@Component({
  selector: 'app-lg-form',
  templateUrl: './lg-form.component.html',
  styleUrls: ['./lg-form.component.scss']
})
export class LGFormComponent implements OnInit {

  lgForm: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private lgServices: LGService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.lgForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required]
      
    });
  }


  onSaveLG() {
    const r1 = this.lgForm.get('r1').value;
    const r2 = this.lgForm.get('r2').value;
    const r3 = this.lgForm.get('r3').value;
    const newLg = new F3(r1, r2, r3);
    this.lgServices.createNewLG(newLg);
    this.router.navigate(['/home/sessionM/session1']);
    
  }
  

  

  

}
