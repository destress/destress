import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-aims1',
  templateUrl: './aims1.component.html',
  styleUrls: ['./aims1.component.scss']
})
export class AimS1Component implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    // do init at here for current route.

    setTimeout(() => {
        this.router.navigate(['home/sessionM/session1']);
    }, 6000);  //5s
    }
}