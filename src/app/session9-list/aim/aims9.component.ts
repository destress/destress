import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-aims9',
  templateUrl: './aims9.component.html',
  styleUrls: ['./aims9.component.scss']
})
export class AimS9Component implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    // do init at here for current route.

    setTimeout(() => {
        this.router.navigate(['home/sessionM/session9']);
    }, 6000);  //5s
    }
}