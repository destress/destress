import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { F4 } from 'src/app/models/F4.model';
import { ASService } from 'src/app/services/as.service';

@Component({
  selector: 'app-as-form',
  templateUrl: './as-form.component.html',
  styleUrls: ['./as-form.component.scss']
})
export class ASFormComponent implements OnInit {

  asForm: FormGroup;
  constructor(private formBuilder: FormBuilder, 
              private asServices: ASService,
              private router: Router) { }
  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.asForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required]
      
    });
  }

  onSaveAS() {
    const r1 = this.asForm.get('r1').value;
    const r2 = this.asForm.get('r2').value;
    const r3 = this.asForm.get('r3').value;
    const r4 = this.asForm.get('r4').value;
    const newAs = new F4(r1, r2, r3, r4);
    this.asServices.createNewAS(newAs);
    this.router.navigate(['/home/sessionM/session9']);
    
  }

  

}
