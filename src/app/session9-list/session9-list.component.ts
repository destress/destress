import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-session9-list',
  templateUrl: './session9-list.component.html',
  styleUrls: ['./session9-list.component.scss']
})
export class Session9ListComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
  }  
  
  onNewAS() {
    this.router.navigate(['/home','sessionM','session9','newas']);
  }
  onNewHC() {
    this.router.navigate(['/home','sessionM','session9','newhc']);
  }
  onNewSS() {
    this.router.navigate(['/home','sessionM','session9','newss']);
  }
  
    
}  