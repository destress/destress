import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-ss',
  templateUrl: './ss.component.html',
  styleUrls: ['./ss.component.scss']
})
export class SSComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    
  }
  onSS() {
    this.router.navigate(['/home/sessionM/session9']);
      
  } 
}