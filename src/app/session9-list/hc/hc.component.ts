import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-hc',
  templateUrl: './hc.component.html',
  styleUrls: ['./hc.component.scss']
})
export class HCComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() { }
  onHC() {
    this.router.navigate(['/home/sessionM/session9']);      
  } 
}