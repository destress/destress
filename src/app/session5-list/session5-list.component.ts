import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-session5-list',
  templateUrl: './session5-list.component.html',
  styleUrls: ['./session5-list.component.scss']
})
export class Session5ListComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
  }  
  
  onNewRE() {
    this.router.navigate(['/home','sessionM','session5','newre']);
  }
  onNewSTR() {
    this.router.navigate(['/home','sessionM','session5','newstr']);
  }
  
    
}  