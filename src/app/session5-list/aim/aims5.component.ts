import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-aims5',
  templateUrl: './aims5.component.html',
  styleUrls: ['./aims5.component.scss']
})
export class AimS5Component implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    // do init at here for current route.

    setTimeout(() => {
        this.router.navigate(['home/sessionM/session5']);
    }, 6000);  //5s
    }
}