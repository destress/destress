import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { STRService } from 'src/app/services/str.service';
import { F3 } from 'src/app/models/F3.model';

@Component({
  selector: 'app-str-form',
  templateUrl: './str-form.component.html',
  styleUrls: ['./str-form.component.scss']
})
export class STRFormComponent implements OnInit {

  strForm: FormGroup;
  

  constructor(private formBuilder: FormBuilder, 
              private leServices: STRService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.strForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],   
      r3: ['', Validators.required],         
    });
  }  

  onSaveSTR() {
    const r1 = this.strForm.get('r1').value; 
    const r2 = this.strForm.get('r2').value;    
    const r3 = this.strForm.get('r3').value;  
    const newStr = new F3(r1, r2, r3);
    this.leServices.createNewSTR(newStr);
    this.router.navigate(['/home/sessionM/session5']);   
  };
}