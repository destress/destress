import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-re',
  templateUrl: './re.component.html',
  styleUrls: ['./re.component.scss']
})
export class REComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    
  }
  onRE() {
    this.router.navigate(['/home/sessionM/session5']);
      
  } 
}