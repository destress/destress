import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  ngOnInit(): void {  }  

  constructor(private router: Router) {}
  
  onGoSession() {
    this.router.navigate(['/home', 'sessionM']);
  }
  onGoLogbook() {
    this.router.navigate(['/home', 'logbookM'])
  }  
  /*onGoDiscovery() {
    this.router.navigate(['/home', 'discoveryM'])
  }
  onGoNetwork() {
    this.router.navigate(['/home', 'networkM'])
  }
  */    
}  