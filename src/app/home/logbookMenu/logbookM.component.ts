import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-logbookM',
  templateUrl: './logbookM.component.html',
  styleUrls: ['./logbookM.component.scss']
})
export class LogbookMComponent implements OnInit{
  ngOnInit(): void { }

  constructor(private router: Router) {}  
  
  onGoLG1() {
    this.router.navigate(['/home','logbookM', 'LG1']);
  }  
 /* onGoLG2() {
    this.router.navigate(['/home','logbookM', 'LG2']);
  }  
  onGoLG3() {
    this.router.navigate(['/home','logbookM', 'LG3']);
  }  */
     
}  