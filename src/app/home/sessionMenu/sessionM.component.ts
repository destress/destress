import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sessionM',
  templateUrl: './sessionM.component.html',
  styleUrls: ['./sessionM.component.scss']
})
export class SessionMListComponent implements OnInit{
  ngOnInit(): void { }

  constructor(private router: Router) {}  
  
  onGoSession1() {
    this.router.navigate(['/home','sessionM', 'aimS1']);
  }  
  onGoSession2() {
    this.router.navigate(['/home','sessionM', 'aimS2']);
  }  
  onGoSession3() {
    this.router.navigate(['/home','sessionM', 'aimS3']);
  }  
  onGoSession4() {
    this.router.navigate(['/home','sessionM', 'aimS4']);
  }  
  onGoSession5() {
    this.router.navigate(['/home','sessionM', 'aimS5']);
  }
  onGoSession6() {
    this.router.navigate(['/home','sessionM', 'aimS6']);
  }  
  onGoSession7() {
    this.router.navigate(['/home','sessionM', 'aimS7']);
  }  
  onGoSession8() {
    this.router.navigate(['/home','sessionM', 'aimS8']);
  }  
  onGoSession9() {
    this.router.navigate(['/home','sessionM', 'aimS9']);
  }    
}  