import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-aims2',
  templateUrl: './aims2.component.html',
  styleUrls: ['./aims2.component.scss']
})
export class AimS2Component implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
    // do init at here for current route.

    setTimeout(() => {
        this.router.navigate(['home/sessionM/session2']);
    }, 6000);  //5s
    }
}