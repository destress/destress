import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-session2-list',
  templateUrl: './session2-list.component.html',
  styleUrls: ['./session2-list.component.scss']
})
export class Session2ListComponent implements OnInit {   

  constructor(private router: Router) {}

  ngOnInit() {
  }  
  
  onNewLG() {
    this.router.navigate(['/home','sessionM','session2','newlg']);
  }
  onNewRT() {
    this.router.navigate(['/home','sessionM','session2','newrt']);
  }
  
    
}  