import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { F5 } from 'src/app/models/F5.model';
import { LG2Service } from 'src/app/services/lg2.service';

@Component({
  selector: 'app-lg2-form',
  templateUrl: './lg2-form.component.html',
  styleUrls: ['./lg2-form.component.scss']
})
export class LG2FormComponent implements OnInit {

  lg2Form: FormGroup;
  


  constructor(private formBuilder: FormBuilder, 
              private lgServices: LG2Service,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.lg2Form = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required],
      r5: ['', Validators.required]
      
    });
  }


  onSaveLG() {
    const r1 = this.lg2Form.get('r1').value;
    const r2 = this.lg2Form.get('r2').value;
    const r3 = this.lg2Form.get('r3').value;
    const r4 = this.lg2Form.get('r4').value;
    const r5 = this.lg2Form.get('r5').value;
    const newLg = new F5(r1, r2, r3, r4, r5);
    this.lgServices.createNewLG(newLg);
    this.router.navigate(['/home/sessionM/session2']);
    
  }
  

  

  

}
