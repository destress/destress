import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { F7 } from 'src/app/models/F7.model';
import { RTService } from 'src/app/services/rt.service';

@Component({
  selector: 'app-rt-form',
  templateUrl: './rt-form.component.html',
  styleUrls: ['./rt-form.component.scss']
})
export class RTFormComponent implements OnInit {

  unForm: FormGroup;
  constructor(private formBuilder: FormBuilder, 
              private rtServices: RTService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.unForm = this.formBuilder.group({
      r1: ['', Validators.required],
      r2: ['', Validators.required],
      r3: ['', Validators.required],
      r4: ['', Validators.required],
      r5: ['', Validators.required],
      r6: ['', Validators.required],
      r7: ['', Validators.required]      
    });
  }
  onSaveRT() {
    const r1 = this.unForm.get('r1').value;
    const r2 = this.unForm.get('r2').value;
    const r3 = this.unForm.get('r3').value;
    const r4 = this.unForm.get('r4').value;
    const r5 = this.unForm.get('r5').value;
    const r6 = this.unForm.get('r6').value;
    const r7 = this.unForm.get('r7').value;
    const newRt = new F7(r1, r2, r3, r4, r5, r6, r7);
    this.rtServices.createNewRT(newRt);
    this.router.navigate(['/home/sessionM/session2']);    
  }
}
